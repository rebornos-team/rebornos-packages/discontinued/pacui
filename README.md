# pacui

Bash script providing advanced Pacman and Yay/Pikaur/Aurman/Pakku/Trizen/Pacaur functionality in a simple UI

https://github.com/excalibur1234/pacui

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/rebornos-packages/pacui.git
```
